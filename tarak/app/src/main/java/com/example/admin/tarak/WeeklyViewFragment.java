package com.example.admin.tarak;

import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import model.CurrentDate;
import model.VisitList;

public class WeeklyViewFragment extends Fragment {

    private RelativeLayout[] daysOfWeek;
    private LinearLayout layout;
    int numDays = 7, dayOfWeek;
    private VisitList visitList;
    private Calendar calendar;
    String[] weekDates = new String[7];
    private SimpleDateFormat sdf;
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.weekly_view, container, false);

        calendar = Calendar.getInstance();
        sdf = new SimpleDateFormat("yyyy-MM-dd");
        daysOfWeek = new RelativeLayout[numDays];
        layout = (LinearLayout) view.findViewById(R.id.layoutDaysOfWeekContainer);
        visitList = MainActivity.visits;

        //Highlight day of week
        dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK) - 1;
        if (dayOfWeek == 0) {
            dayOfWeek = 7;
        }
        for (int i = 0; i < numDays; i++) {
            daysOfWeek[i] = (RelativeLayout)layout.getChildAt(i);
        }
        daysOfWeek[dayOfWeek - 1].setBackgroundColor(getResources().getColor(R.color.transparentWhite));

        setCurrentWeekDays();

        if(visitList != null) {
            displayVisits();
        }

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);

        //Set on click action
        for (int i = 0; i < numDays; i++) {
            final int e = i;
            daysOfWeek[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Date date = new Date();
                    try {
                        date = sdf.parse(weekDates[e]);
                    } catch (ParseException e1) {
                        e1.printStackTrace();
                    }
                    calendar.setTime(date);
                    CurrentDate currentDate = new CurrentDate();
                    currentDate.setDay(calendar.get(Calendar.DAY_OF_MONTH));
                    //well + 1 because of JAVA
                    currentDate.setMonth(calendar.get(Calendar.MONTH) + 1);
                    currentDate.setYear(calendar.get(Calendar.YEAR));

                    //set active daily tab
                    MainActivity.currentDate = currentDate;
                    MainActivity.viewPager.setCurrentItem(1, true);
                }
            });
        }
    }

    private void setCurrentWeekDays() {
        calendar.set(Calendar.DAY_OF_WEEK, calendar.getFirstDayOfWeek());
        for (int i = 0; i < 7; i++) {
            System.out.println("WeekDate: " + sdf.format(calendar.getTime()));
            weekDates[i] = sdf.format(calendar.getTime());
            calendar.add(Calendar.DAY_OF_WEEK, 1);
        }
    }

    private void displayVisits() {
        for(int i = 0; i < visitList.getVisits().size(); i++) {
            String visitDate = visitList.getVisits().get(i).getVisitDate();
            for (int r = 0; r < 7; r++) {
                if(weekDates[r].equals(visitDate)) {
                    String visitHour = visitList.getVisits().get(i).getCheckInTime().substring(0, 2);
                    System.out.println("VisitInWeek: " + weekDates[r] + " " + visitDate);
                    String visitorName = visitList.getVisits().get(i).getVisitorName();
                    if(visitorName.length() > 4) {
                        visitorName = visitorName.substring(0, 4);
                    }
                    visitorName = visitorName  + "\n" + "...";

                    TextView visitView = new TextView(getContext());
                    visitView.setText(visitorName);
                    visitView.setTextColor(getResources().getColor(R.color.darkgrey));
                    GradientDrawable gd = new GradientDrawable();
                    gd.setColor(getResources().getColor(R.color.grey));
                    gd.setStroke(3, getResources().getColor(R.color.green));
                    visitView.setBackgroundDrawable(gd);
                    visitView.setPadding(8, 8, 8, 8);
                    visitView.setGravity(Gravity.CENTER);
                    daysOfWeek[r].addView(visitView, new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, 114));
                    ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) visitView.getLayoutParams();
                    params.topMargin = 3 + 120 * Integer.parseInt(visitHour);
                }
            }
        }
    }
}

