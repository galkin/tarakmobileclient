package com.example.admin.tarak;

import android.content.Context;
import android.graphics.drawable.Drawable;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.DayViewDecorator;
import com.prolificinteractive.materialcalendarview.DayViewFacade;

public class DayActionDecorator implements DayViewDecorator {

    private final Drawable drawable;

    public DayActionDecorator(Context context) {
        drawable = context.getResources().getDrawable(R.drawable.month_day_action);
    }

    @Override
    public boolean shouldDecorate(CalendarDay day) {
        return true;
    }

    @Override
    public void decorate(DayViewFacade view) {
        view.setSelectionDrawable(drawable);
    }
}