package com.example.admin.tarak;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.TimePickerDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.admin.tarak.databinding.FormDialogBinding;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.TimeZone;

import api.RetrofitSingleton;
import api.VisitService;
import util.JSONVisitParser;
import model.Visit;
import model.VisitModel;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import util.SharedPreferencesHelper;
import api.URI;

public class FormDialogActivity extends Activity implements OnClickListener {

    Button dateButton, fromTimeButton, toTimeButton, buttonSave, buttonClose, buttonDelete;
    EditText editDescription;
    AutoCompleteTextView acVisitorName;
    String contactName, currentDate, currentTime;
    ArrayList<String> contactNames = new ArrayList<String>();
    private DatePickerDialog datePickerDialog;
    private TimePickerDialog fromTimePickerDialog, toTimePickerDialog;
    private SimpleDateFormat dateFormatter, timeFormatter;
    private DecimalFormat decimalFormatter;
    final Calendar calendar = Calendar.getInstance();
    private Visit currentVisit;
    private SharedPreferencesHelper sharedPreferencesHelper;
    String tz;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.form_dialog);

        tz = TimeZone.getDefault().getID();
        dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
        timeFormatter = new SimpleDateFormat("HH:mm");
        decimalFormatter = new DecimalFormat("00");

        currentDate = dateFormatter.format(calendar.getTime());
        currentTime = timeFormatter.format(calendar.getTime());

        currentVisit = new Visit();
        currentVisit.setVisitDate(currentDate);
        currentVisit.setCheckInTime(currentTime);
        currentVisit.setCheckOutTime(currentTime);
        currentVisit.setTimeZoneInfo(tz);
        currentVisit.setDescription("");

        sharedPreferencesHelper = new SharedPreferencesHelper(this);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            if(extras.getString("visitToEdit") != null) {
                String jsonVisit = extras.getString("visitToEdit");
                currentVisit = JSONVisitParser.getVisit(jsonVisit);
            }

            if(extras.getString("contactName") != null) {
                contactName = extras.getString("contactName");
                currentVisit.setVisitorName(contactName);
            }
        }

        //binding
        FormDialogBinding binding = DataBindingUtil.setContentView(this, R.layout.form_dialog);
        binding.setVisit(currentVisit);

        findControlsById();

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, getContactName(this.getContentResolver()));
        acVisitorName = (AutoCompleteTextView) findViewById(R.id.acContactName);
        acVisitorName.setAdapter(adapter);

        setDateTimeField();
    }

    private void findControlsById() {

        buttonSave = (Button) findViewById(R.id.buttonSave);
        buttonClose = (Button) findViewById(R.id.buttonClose);
        buttonDelete = (Button) findViewById(R.id.buttonDelete);
        acVisitorName = (AutoCompleteTextView) findViewById(R.id.acContactName);
        dateButton = (Button) findViewById(R.id.buttonDateEvent);
        fromTimeButton = (Button) findViewById(R.id.buttonFromTime);
        toTimeButton = (Button) findViewById(R.id.buttonToTime);
        editDescription = (EditText) findViewById(R.id.editDescription);

        buttonSave.setOnClickListener(this);
        buttonClose.setOnClickListener(this);
        buttonDelete.setOnClickListener(this);
    }

    public ArrayList getContactName(ContentResolver cr) {
        Cursor phones = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,null,null, null);
        // use the cursor to access the contacts
        while (phones.moveToNext())
        {
            contactName = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            contactNames.add(contactName);
        }
        return contactNames;
    }

    private void setDateTimeField() {

        //Set DatePicker on Date Button
        dateButton.setOnClickListener(this);
        datePickerDialog = new DatePickerDialog(this, TimePickerDialog.THEME_HOLO_LIGHT,
                new OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                calendar.set(year, monthOfYear, dayOfMonth);
                dateButton.setText(dateFormatter.format(calendar.getTime()));
            }

        },calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

       //Set TimePicker on FromTimeButton
        fromTimeButton.setOnClickListener(this);
        fromTimePickerDialog = new TimePickerDialog(this, TimePickerDialog.THEME_HOLO_LIGHT,
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        fromTimeButton.setText(String.valueOf(decimalFormatter.format(hourOfDay)) + ":" + String.valueOf(decimalFormatter.format(minute)));
                    }
                }, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), true);
        fromTimePickerDialog.setTitle(getResources().getString(R.string.set_time_dialog));

        //Set TimePicker on ToTimeButton
        toTimeButton.setOnClickListener(this);
        toTimePickerDialog = new TimePickerDialog(this, TimePickerDialog.THEME_HOLO_LIGHT,
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        toTimeButton.setText(String.valueOf(decimalFormatter.format(hourOfDay)) + ":" + String.valueOf(decimalFormatter.format(minute)));
                    }
                }, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), true);
        toTimePickerDialog.setTitle(getResources().getString(R.string.set_time_dialog));

    }

    private Visit deleteVisit(){
        if(currentVisit != null && currentVisit.getId() > 0) {
            apiVisitAction("delete");
        }

        return currentVisit;
    }

    private Visit addOrUpdateVisit(){
        if(currentVisit.getId() == 0) {
            apiVisitAction("add");
        }
        else{
            apiVisitAction("update");
        }

        return currentVisit;
    }

    private void apiVisitAction (String action) {
        try {

            VisitModel visitModel = new VisitModel();
            visitModel.setId(currentVisit.getId());
            visitModel.setDescription(currentVisit.getDescription());
            visitModel.setTimeZoneInfo(tz);
            visitModel.setVisitorName(currentVisit.getVisitorName());
            visitModel.setCheckInDate(currentVisit.getVisitDate() + " " + currentVisit.getCheckInTime() + ":00");
            visitModel.setCheckOutDate(currentVisit.getVisitDate() + " " + currentVisit.getCheckOutTime() + ":00");

            action = action.toLowerCase();

            String accessToken = sharedPreferencesHelper.getString("AccessToken");
            String bearer = "Bearer " + accessToken;

            Retrofit retrofit = RetrofitSingleton.getInstance(URI.BASE_URL);
            VisitService visitService = retrofit.create(VisitService.class);
            Call<Void> call = null;
            switch (action) {
                case "add":
                    call = visitService.addVisit(bearer, visitModel);
                    break;
                case "update":
                    call = visitService.updateVisit(bearer, visitModel);
                    break;
                case "delete":
                    call = visitService.deleteVisit(bearer, visitModel.getId());
                    break;
            }

            if (call != null) {
                call.clone().enqueue(new Callback<Void>() {
                    @Override
                    public void onResponse(Call<Void> call, Response<Void> response) {
                        // response.isSuccessful() is true if the response code is 2xx
                        if (response.isSuccessful()) {


                        } else {
                            int statusCode = response.code();
                            // handle request errors yourself
                            ResponseBody errorBody = response.errorBody();

                            Context context = getApplicationContext();
                            CharSequence text = "Failed to get visit data! " + errorBody;
                            int duration = Toast.LENGTH_LONG;

                            Toast.makeText(context, text, duration).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Void> call, Throwable t) {
                        // handle execution failures like no internet connectivity
                        currentVisit = null;

                        Context context = getApplicationContext();
                        CharSequence text = "Failed to connect!";
                        int duration = Toast.LENGTH_LONG;

                        Toast.makeText(context, text, duration).show();
                    }
                });
            }
            else{
                Context context = getApplicationContext();
                CharSequence text = "Invalid action!";
                int duration = Toast.LENGTH_LONG;

                Toast.makeText(context, text, duration).show();
            }
        } catch (Exception e) {
            Context context = getApplicationContext();
            int duration = Toast.LENGTH_LONG;

            Toast.makeText(context, e.toString(), duration).show();
        }
    }

    @Override
    public void onClick(View view) {
        Intent intent = getIntent();
        if(view == dateButton) {
            datePickerDialog.show();
        } else if (view == fromTimeButton){
            fromTimePickerDialog.show();
        } else if (view == toTimeButton){
            toTimePickerDialog.show();
        }
        else if (view == buttonDelete){
            Visit deletedVisit = deleteVisit();
            Intent _result = new Intent();
            if(deletedVisit != null) {
                _result.putExtra("visitToDelete", deletedVisit.toJSON());
            }
            setResult(Activity.RESULT_OK, _result);
            finish();
        } else if (view == buttonSave) {
            Visit newVisit = addOrUpdateVisit();
            Intent _result = new Intent();
            _result.putExtra("visitToAdd", newVisit.toJSON());
            setResult(Activity.RESULT_OK, _result);
            finish();
        } else if (view == buttonClose) {
            setResult(RESULT_CANCELED, intent);
            finish();
        }
    }
}
