package com.example.admin.tarak;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import model.VisitList;


public class YearlyViewAdapter extends BaseAdapter implements OnDateSelectedListener {
    Context context;
    List<Integer> months;
    long date;
    int year, month;
    MaterialCalendarView mCalendarView;
    private VisitList visitList;
    private SimpleDateFormat dateFormat;
    private Calendar calendar;
    private final CurrentDayDecorator currentDayDecorator = new CurrentDayDecorator();

    YearlyViewAdapter(Context context, List<Integer> months, int year) {
        this.context = context;
        this.months = months;
        this.year = year;
    }
    @Override
    public int getCount() {
        return months.size();
    }

    @Override
    public Object getItem(int position) {
        return months.get(position);
    }

    @Override
    public long getItemId(int position) {
        return months.indexOf(getItem(position));
    }
     @Override
    public int getViewTypeCount() {
        return getCount();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context
                    .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.yearly_view_item, null);
        }

        mCalendarView = (MaterialCalendarView) convertView.findViewById(R.id.yearlyCalendarMonth);
        month = months.get(position);
        calendar = Calendar.getInstance();
        dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        visitList = MainActivity.visits;

        //Set date
        setDate(mCalendarView, month, year);

        //display Visits
        if(visitList != null) {
            displayVisits();
        }
        mCalendarView.setOnDateChangedListener(this);
        mCalendarView.setShowOtherDates(0);
        mCalendarView.setArrowColor(context.getResources().getColor(R.color.green));

        return convertView;
    }

    //Calendar set date
    public void setDate(MaterialCalendarView mCalendarView, int month, int year) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.YEAR, year);
        date = calendar.getTimeInMillis();
        mCalendarView.setCurrentDate(calendar);
    }

    @Override
    public void onDateSelected(@NonNull MaterialCalendarView mCalendarView, @Nullable CalendarDay date, boolean selected) {
        MainActivity.yDate = mCalendarView.getSelectedDate();
        System.out.println("DateFromYear " + MainActivity.yDate + " " + mCalendarView.getSelectedDate());
        MainActivity.viewPager.setCurrentItem(3, true);
    }

    private void displayVisits() {
        Collection<CalendarDay> visitDates = new ArrayList<CalendarDay>();
        for(int i = 0; i < visitList.getVisits().size(); i++) {
            System.out.println("VISIT " + i + " : " + visitList.getVisits().get(i) + " " +
                    visitList.getVisits().get(i).getVisitDate());
            String visitDate = visitList.getVisits().get(i).getVisitDate();
            Date convertedVisitDate = new Date();
            try {
                convertedVisitDate = dateFormat.parse(visitDate);
                if(visitDates.contains(convertedVisitDate)) {
                    i++;
                }
                else {
                    visitDates.add(CalendarDay.from(convertedVisitDate));
                }
            }
            catch (ParseException e) {
                e.printStackTrace();
            }
        }

        mCalendarView.addDecorators(
                new DayActionDecorator(context),
                new EventDecorator(context, visitDates),
                currentDayDecorator
        );

    }


}
