package com.example.admin.tarak;

import android.app.Activity;
import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import util.JSONVisitParser;
import model.CurrentDate;
import model.DailyViewItem;
import model.ICommonFragment;
import model.Visit;
import model.VisitList;
import util.DateHelper;

public class DailyViewFragment extends ListFragment implements AdapterView.OnItemClickListener, ICommonFragment {

    String[] stringHours =
            {"00:00", "01:00", "02:00", "03:00", "04:00", "05:00", "06:00", "07:00",
                    "08:00", "09:00", "10:00", "11:00", "12:00", "13:00", "14:00", "15:00",
                    "16:00", "17:00", "18:00", "19:00", "20:00", "21:00", "22:00", "23:00"};

    TypedArray menuIcons;
    DailyViewAdapter dailyViewAdapter;

    private List<DailyViewItem> dailyViewItems;
    private SimpleDateFormat timeFormatter;
    private VisitList visitList;
    private CurrentDate currentDate;

    public DailyViewFragment(){
        this.currentDate = new CurrentDate();
    }

    public void setVisitList() {
        this.visitList = MainActivity.visits;
        setDailyView();
    }

    private String getVisitDescription(int currentMonth, int currentDay, int currentHour) {
        if (visitList != null) {
            try {
                List<Visit> visits = visitList.getVisits();
                //SimpleDateFormat isoFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                //TODO: Set timezone of user device
                //isoFormat.setTimeZone(TimeZone.getTimeZone("America/New_York"));

                String[] dateFormats = DateHelper.getDateFromats();

                for (int i = 0; i < visits.size(); i++) {
                    Visit visit = visits.get(i);
                    String visitDateTime = visit.getVisitDate() + " " + visit.getCheckInTime()  + ":00";
                    Date date = DateHelper.parseDate(visitDateTime, dateFormats);

                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(date);
                    int hour = calendar.get(Calendar.HOUR_OF_DAY);
                    int day = calendar.get(Calendar.DAY_OF_MONTH);
                    //we need to add 1 to make month number more understandable
                    int month = calendar.get(Calendar.MONTH) + 1;

                    if (currentMonth == month && currentDay == day && hour == currentHour)
                        return visits.get(i).getDescription().toString();
                }
            }
            catch(Exception ex){
               Log.v("", ex.toString());
            }
        }
        return "*";
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        currentDate = MainActivity.currentDate;

        return inflater.inflate(R.layout.daily_view, null, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.visitList = MainActivity.visits;
        setDailyView();
    }

    private void setDailyView() {
        menuIcons = getResources().obtainTypedArray(R.array.add_icon);
        dailyViewItems = new ArrayList<DailyViewItem>();

        for (int hour = 0; hour < 24; hour++) {
            String description = getVisitDescription(currentDate.getMonth(), currentDate.getDay(), hour);
            String timeFrame = hour < 23 ? stringHours[hour] + "\n" + stringHours[hour + 1] : stringHours[hour] + "\n" + stringHours[0];
            String checkInHour = stringHours[hour];
            String checkOutHour = hour < 23 ?stringHours[hour + 1] : stringHours[0];

            DailyViewItem items = new DailyViewItem(timeFrame, menuIcons.getResourceId(0, -1), description, checkInHour, checkOutHour);
            dailyViewItems.add(items);
        }

        //Set adapter
        dailyViewAdapter = new DailyViewAdapter(getActivity(), dailyViewItems);
        setListAdapter(dailyViewAdapter);
        getListView().setOnItemClickListener(this);

        //Set position to current time
        timeFormatter = new SimpleDateFormat("HH");
        //TODO: Setup proper timezone
        timeFormatter.setTimeZone(TimeZone.getTimeZone("Asia/Almaty"));
        String hour = timeFormatter.format(new Date());
        getListView().setSelection(Integer.parseInt(hour));
    }

    //Set OnClick Listener to each hour
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intent = new Intent(getActivity(), FormDialogActivity.class);

        DailyViewItem v = (DailyViewItem) parent.getAdapter().getItem(position);
        if (visitList != null) {
            Visit visit = visitList.getVisitByTimeFrame(currentDate.getMonth(),
                    currentDate.getDay(),
                    v.getCheckinHour(),
                    v.getCheckoutHour());

            if (visit != null) {
                intent.putExtra("visitToEdit", visit.toJSON());
            }
        }

        DailyViewFragment.this.startActivityForResult(intent, 1);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                Bundle b = data.getExtras();
                if (b != null) {
                    addVisitToVisitList(b);
                    deleteVisitToVisitList(b);
                }
            } else if (resultCode == 0) {
                System.out.println("RESULT CANCELLED");
            }
        }
    }

    private void addVisitToVisitList(Bundle b) {
        String newVisitItem = (String) b.getSerializable("visitToAdd");
        if(newVisitItem != null) {
            //adding new visit
            Visit newVisit = JSONVisitParser.getVisit(newVisitItem);
            visitList.addVisit(newVisit);
            setDailyView();
        }
    }

    private void deleteVisitToVisitList(Bundle b) {
        String deletedVisitItem = (String) b.getSerializable("visitToDelete");
        if(deletedVisitItem != null) {
            //adding new visit
            Visit deletedVisit = JSONVisitParser.getVisit(deletedVisitItem);
            visitList.deleteVisit(deletedVisit);
            setDailyView();
        }
    }

}