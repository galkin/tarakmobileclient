package com.example.admin.tarak;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

import model.CurrentDate;
import model.VisitList;


public class MonthlyViewFragment extends Fragment implements OnDateSelectedListener {
    private Calendar calendar;
    private SimpleDateFormat dateFormat;
    private VisitList visitList;
    MaterialCalendarView mCalendarView;
    private final CurrentDayDecorator currentDayDecorator = new CurrentDayDecorator();
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.monthly_view, container, false);
        mCalendarView = (MaterialCalendarView) view.findViewById(R.id.calendarView11);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);
        visitList = MainActivity.visits;
        calendar = Calendar.getInstance();
        dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        //display Visits
        if(visitList != null) {
            displayVisits();
        }

        mCalendarView.setShowOtherDates(0);
        mCalendarView.setOnDateChangedListener(this);

    }

    private void displayVisits() {
        Collection<CalendarDay> visitDates = new ArrayList<CalendarDay>();
        for(int i = 0; i < visitList.getVisits().size(); i++) {
            System.out.println("VISIT " + i + " : " + visitList.getVisits().get(i) + " " +
                    visitList.getVisits().get(i).getVisitDate());
            String visitDate = visitList.getVisits().get(i).getVisitDate();
            Date convertedVisitDate = new Date();
            try {
                convertedVisitDate = dateFormat.parse(visitDate);
                if(visitDates.contains(convertedVisitDate)) {
                    i++;
                }
                else {
                    visitDates.add(CalendarDay.from(convertedVisitDate));
                }
            }
            catch (ParseException e) {
                e.printStackTrace();
            }
        }
        mCalendarView.addDecorators(
                new DayActionDecorator(getActivity()),
                new EventDecorator(getActivity(), visitDates),
                currentDayDecorator
        );
    }

    @Override
    public void onDateSelected(@NonNull MaterialCalendarView mCalendarView, @Nullable CalendarDay date, boolean selected) {
        Calendar calendar = Calendar.getInstance();
        Date convertedDate = new Date();
        try {
            convertedDate = dateFormat.parse(getSelectedDatesString());
        }
        catch (ParseException e) {
            e.printStackTrace();
        }
        calendar.setTime(convertedDate);
        CurrentDate currentDate = new CurrentDate();
        currentDate.setDay(calendar.get(Calendar.DAY_OF_MONTH));
        //well + 1 because of JAVA
        currentDate.setMonth(calendar.get(Calendar.MONTH) + 1);
        currentDate.setYear(calendar.get(Calendar.YEAR));

        //set active daily tab
        MainActivity.currentDate = currentDate;
        MainActivity.viewPager.setCurrentItem(1, true);
    }

    private String getSelectedDatesString() {
        CalendarDay date = mCalendarView.getSelectedDate();
        if (date == null) {
            return "No Selection";
        }
        return dateFormat.format(date.getDate());
    }

    @Override
    public void onStart() {
        super.onStart();
        System.out.println("ONStart");
    }

    @Override
    public void onResume() {
        super.onResume();
        System.out.println("ONResume");
        if(MainActivity.yDate != null) {
            mCalendarView.setCurrentDate(MainActivity.yDate);
        }
        else {
            mCalendarView.setCurrentDate(Calendar.getInstance());
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        System.out.println("ONDestroy");
    }
}

