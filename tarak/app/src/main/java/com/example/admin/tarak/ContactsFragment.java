package com.example.admin.tarak;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import model.SelectUser;
import model.SelectUserAdapter;
import model.Visit;
import model.VisitList;
import util.JSONVisitParser;

public class ContactsFragment extends Fragment implements AdapterView.OnItemClickListener {
     ArrayList<SelectUser> selectUsers;
     List<SelectUser> temp;
     ListView listView;
     Cursor phones;

     ContentResolver resolver;
     TextView noContacts;
     SelectUserAdapter adapter;
    private VisitList visitList;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.contact_list_view, null, false);

        selectUsers = new ArrayList<SelectUser>();
        resolver = getContext().getContentResolver();
        listView = (ListView) view.findViewById(R.id.contacts_list);
        noContacts = (TextView) view.findViewById(R.id.no_contacts);
        phones = getContext().getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC");
        MultiAsyncTask.execute(new LoadContact());
        listView.setOnItemClickListener(this);

        this.visitList = MainActivity.visits;
        return view;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        TextView textView = (TextView) view.findViewById(R.id.name);
        String contactName = textView.getText().toString();
        Intent intent = new Intent(getActivity(), FormDialogActivity.class);
        intent.putExtra("contactName", contactName);
        ContactsFragment.this.startActivityForResult(intent, 1);
    }

    // Load data on background
    class LoadContact extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            // Get Contact list from Phone
            if (phones != null) {
                Log.e("count", "" + phones.getCount());
                if (phones.getCount() == 0) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            noContacts.setText("Нет контактов");
                        }
                    });
                }

                while (phones.moveToNext()) {
                    Bitmap bit_thumb = null;
                    String id = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID));
                    String name = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                    String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                    String image_thumb = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_THUMBNAIL_URI));
                    try {
                        if (image_thumb != null) {
                            bit_thumb = MediaStore.Images.Media.getBitmap(resolver, Uri.parse(image_thumb));
                        } else {
                            Log.v("No Image Thumb", "--------------");
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    SelectUser selectUser = new SelectUser();
                    selectUser.setThumb(bit_thumb);
                    selectUser.setName(name);
                    selectUser.setPhone(phoneNumber);
                    selectUsers.add(selectUser);
                }
            } else {
                Log.e("Cursor close 1", "----------------");
            }

            phones.close();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            adapter = new SelectUserAdapter(selectUsers, getContext());
            listView.setAdapter(adapter);
            listView.setDivider(new ColorDrawable(0x84FFFFFF));
            listView.setDividerHeight(1);
            listView.setFastScrollEnabled(true);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        //phones.close();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                Bundle b = data.getExtras();
                if (b != null) {
                    addVisitToVisitList(b);
                    deleteVisitToVisitList(b);
                }
            } else if (resultCode == 0) {
                System.out.println("RESULT CANCELLED");
            }
        }
    }

    private void addVisitToVisitList(Bundle b) {
        String newVisitItem = (String) b.getSerializable("visitToAdd");
        if(newVisitItem != null) {
            //adding new visit
            Visit newVisit = JSONVisitParser.getVisit(newVisitItem);
            visitList.addVisit(newVisit);

        }
    }

    private void deleteVisitToVisitList(Bundle b) {
        String deletedVisitItem = (String) b.getSerializable("visitToDelete");
        if(deletedVisitItem != null) {
            //adding new visit
            Visit deletedVisit = JSONVisitParser.getVisit(deletedVisitItem);
            visitList.deleteVisit(deletedVisit);
        }
    }
}