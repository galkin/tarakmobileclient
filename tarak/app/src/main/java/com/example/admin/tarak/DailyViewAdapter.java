package com.example.admin.tarak;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import model.DailyViewItem;

public class DailyViewAdapter extends BaseAdapter {

    Context context;
    List<DailyViewItem> dailyViewItems;
    final Calendar calendar = Calendar.getInstance();
    private SimpleDateFormat timeFormatter;

    DailyViewAdapter(Context context, List<DailyViewItem> dailyViewItems) {
        this.context = context;
        this.dailyViewItems = dailyViewItems;
    }

    @Override
    public int getCount() {
        return dailyViewItems.size();
    }

    @Override
    public Object getItem(int position) {
        return dailyViewItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return dailyViewItems.indexOf(getItem(position));
    }

    @Override
    public int getViewTypeCount() {
        return getCount();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        DailyViewItem dailyViewItem = dailyViewItems.get(position);

        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            RelativeLayout layout = (RelativeLayout)mInflater.inflate(R.layout.daily_view_item, null);

            showVisit(layout, dailyViewItem.getMessage());

            convertView = layout;
        }

        ImageView imgIcon = (ImageView) convertView.findViewById(R.id.add_icon);
        TextView txtHour = (TextView) convertView.findViewById(R.id.daily_hour);

        imgIcon.setImageResource(dailyViewItem.getIcon());
        txtHour.setText(dailyViewItem.getHour());

        //Show current time
        showCurrentTime(dailyViewItem, convertView);

        return convertView;
    }

    //Show current time
    public void showCurrentTime(DailyViewItem row_pos, View convertView) {
        timeFormatter = new SimpleDateFormat("HH:00");
        String currentTime = timeFormatter.format(calendar.getTime());
        int minute = calendar.get(Calendar.MINUTE);
        if (currentTime.equals(row_pos.getCheckinHour())) {
            View currentTimeLine = convertView.findViewById(R.id.current_time);
            currentTimeLine.setVisibility(View.VISIBLE);
            ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) currentTimeLine.getLayoutParams();
            params.topMargin = minute;
        }
    }

    //Show visits
    public void showVisit(RelativeLayout layout, String message) {
        TextView dailyVisit = new TextView(context);
        dailyVisit.setText(message);
        layout.addView(dailyVisit);
        dailyVisit.getLayoutParams().width = ViewGroup.LayoutParams.MATCH_PARENT;
        dailyVisit.getLayoutParams().height = 140;
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) dailyVisit.getLayoutParams();
        params.leftMargin = 115;
        params.rightMargin = 115;
        params.topMargin = 10;
        params.bottomMargin = 10;
        if(message.length() > 1) {
            dailyVisit.setBackgroundColor(context.getResources().getColor(R.color.transparentWhite));
            dailyVisit.setTextColor(context.getResources().getColor(R.color.darkgrey));
            dailyVisit.setPadding(15, 5, 15, 5);
        }
    }
}