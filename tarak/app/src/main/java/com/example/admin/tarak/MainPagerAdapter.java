package com.example.admin.tarak;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.List;

import model.CurrentDate;
import model.ICommonFragment;
import model.Visit;
import model.VisitList;

public class MainPagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    private ICommonFragment fragment;
    //private CurrentDate currentDate;

    public void setVisitList(VisitList visitList) {
        if(fragment!= null)
            fragment.setVisitList();
    }

    public MainPagerAdapter(FragmentManager fm, int NumOfTabs, CurrentDate currentDate) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
        //this.currentDate = currentDate;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                ContactsFragment contactsTab = new ContactsFragment();
                return contactsTab;
            case 1:
                DailyViewFragment dailyViewTab = new DailyViewFragment();
                //Bundle bundle = new Bundle();
                //bundle.putSerializable("current_date", currentDate);
                //dailyViewTab.setArguments(bundle);

                fragment = dailyViewTab;
                return dailyViewTab;
            case 2:
                WeeklyViewFragment weeklyViewTab = new WeeklyViewFragment();
                return weeklyViewTab;
            case 3:
                MonthlyViewFragment monthlyViewTab = new MonthlyViewFragment();
                return monthlyViewTab;
            case 4:
                YearlyViewFragment yearlyViewTab = new YearlyViewFragment();
                return yearlyViewTab;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }

    @Override
    public int getItemPosition(Object object) {
        // POSITION_NONE makes it possible to reload the PagerAdapter
        return POSITION_NONE;
    }
}