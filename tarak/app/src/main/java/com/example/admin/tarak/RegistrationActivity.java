package com.example.admin.tarak;


import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.admin.tarak.databinding.RegistrationViewBinding;

import api.LoginService;
import api.RetrofitSingleton;
import model.User;
import model.UserModel;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import api.URI;

/**
 * Created by Kristina on 21.04.2016.
 */
public class RegistrationActivity extends AppCompatActivity implements View.OnClickListener {

    private User currentUser;
    Button buttonRegistration;
    Button buttonLogin;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registration_view);

        context = this;

        currentUser = new User();
        currentUser.setEmail("");
        currentUser.setFirstName("");
        currentUser.setLastName("");
        currentUser.setPassword("");

        //binding
        RegistrationViewBinding binding = DataBindingUtil.setContentView(this, R.layout.registration_view);
        binding.setUser(currentUser);

        //Toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Remove toolbar title and set logotip
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setIcon(R.drawable.logotip);

        buttonRegistration = (Button) findViewById(R.id.buttonRegistration);
        buttonRegistration.setOnClickListener(this);

        buttonLogin = (Button) findViewById(R.id.buttonLogin);
        buttonLogin.setOnClickListener(this);
    }

    @Override
    public void onClick(View arg0) {
        if (arg0.getId() == R.id.buttonRegistration) {
            addUser(context);
            buttonRegistration.setEnabled(false);
            buttonLogin.setEnabled(false);
        }
        else if (arg0.getId() == R.id.buttonLogin) {
            Intent intent = new Intent(this, LoginActivity.class);
            this.startActivity(intent);
        }
    }

    private void addUser(final Context context){
        try {

            UserModel userModel = new UserModel();
            userModel.setEmail(currentUser.getEmail());
            userModel.setFirstName(currentUser.getFirstName());
            userModel.setLastName(currentUser.getLastName());
            userModel.setPassword(currentUser.getPassword());
            //TODO: Add confirm password
            userModel.setConfirmPassword(currentUser.getPassword());
            userModel.setPhoneNumber(currentUser.getPhoneNumber());

            Retrofit retrofit = RetrofitSingleton.getInstance(URI.BASE_URL);
            LoginService loginService = retrofit.create(LoginService.class);
            Call<Void> createUserCall = loginService.createUser(userModel);

            createUserCall.enqueue(new Callback<Void>() {
                @Override
                public void onResponse(Call<Void> call, Response<Void> response) {
                    buttonRegistration.setEnabled(true);
                    buttonLogin.setEnabled(true);

                    // response.isSuccessful() is true if the response code is 2xx
                    if (response.isSuccessful()) {
                        Intent intent = new Intent(context, LoginActivity.class);
                        context.startActivity(intent);

                    } else {
                        int statusCode = response.code();
                        // handle request errors yourself
                        ResponseBody errorBody = response.errorBody();

                        Context context = getApplicationContext();
                        CharSequence text = "Failed to create user! " + statusCode;
                        int duration = Toast.LENGTH_LONG;

                        Toast.makeText(context, text, duration).show();
                    }
                }

                @Override
                public void onFailure(Call<Void> call, Throwable t) {
                    // handle execution failures like no internet connectivity
                    buttonRegistration.setEnabled(true);
                    buttonLogin.setEnabled(true);

                    Context context = getApplicationContext();
                    CharSequence text = "Failed to connect!";
                    int duration = Toast.LENGTH_LONG;

                    Toast.makeText(context, text, duration).show();
                }
            });
        }
        catch(Exception e){
            String s = e.toString();
        }
    }
}
