package com.example.admin.tarak;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Arrays;
import java.util.Calendar;
import java.util.List;


public class YearlyViewFragment extends Fragment {
    GridView yearContainer;
    YearlyViewAdapter adapter;
    List<Integer> months = Arrays.asList(Calendar.JANUARY, Calendar.FEBRUARY, Calendar.MARCH, Calendar.APRIL, Calendar.MAY,
            Calendar.JUNE, Calendar.JULY, Calendar.AUGUST, Calendar.SEPTEMBER, Calendar.OCTOBER, Calendar.NOVEMBER,
            Calendar.DECEMBER);
    ImageButton prevYear, nextYear;
    TextView textYear;
    int year;
    Calendar calendar;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.yearly_view, null, false);
        yearContainer = (GridView) view.findViewById(R.id.yearlyCalendar);
        prevYear = (ImageButton) view.findViewById(R.id.buttonPrevYear);
        nextYear = (ImageButton) view.findViewById(R.id.buttonNextYear);
        textYear = (TextView) view.findViewById(R.id.textYear);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);

        //Get current year
        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);

        //Set adapter
        adapter = new YearlyViewAdapter(getActivity(), months, year);
        yearContainer.setAdapter(adapter);


        //Set year to texyview
        textYear.setText(String.valueOf(year));

        //Set OnClick Listener to previous year button
        prevYear.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (year < 1930) {
                    Toast toast = Toast.makeText(getActivity().getApplicationContext(),
                            "Минимальная дата", Toast.LENGTH_SHORT);
                    toast.show();
                } else {
                    year -= 1;
                    textYear.setText(String.valueOf(year));
                    adapter = new YearlyViewAdapter(getActivity(), months, year);
                    adapter.notifyDataSetChanged();
                    yearContainer.setAdapter(adapter);
                }
            }
        });

        //Set OnClick Listener to next year button
        nextYear.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if(year > 2030) {
                    Toast toast = Toast.makeText(getActivity().getApplicationContext(),
                            "Максимальная дата", Toast.LENGTH_SHORT);
                    toast.show();
                } else {
                    year += 1;
                    textYear.setText(String.valueOf(year));
                    adapter = new YearlyViewAdapter(getActivity(), months, year);
                    adapter.notifyDataSetChanged();
                    yearContainer.setAdapter(adapter);
                }
            }
        });
    }

}
