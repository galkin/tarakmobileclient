package com.example.admin.tarak;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.admin.tarak.databinding.LoginViewBinding;

import api.LoginService;
import api.RetrofitSingleton;
import model.AccessToken;
import model.User;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import util.SharedPreferencesHelper;
import api.URI;

/**
 * Created by Kristina on 21.04.2016.
 */
public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private User currentUser;
    private AccessToken accessToken;
    private SharedPreferencesHelper sharedPreferencesHelper;
    private Button btnLogin;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_view);

        currentUser = new User();
        currentUser.setEmail("");
        currentUser.setFirstName("");
        currentUser.setLastName("");
        currentUser.setPassword("");

        sharedPreferencesHelper = new SharedPreferencesHelper(this);

        LoginViewBinding binding = DataBindingUtil.setContentView(this, R.layout.login_view);
        binding.setUser(currentUser);

        //Toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Remove toolbar title and set logo
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setIcon(R.drawable.logotip);

        btnLogin = (Button) findViewById(R.id.buttonLogin2);
        btnLogin.setOnClickListener(this);
    }

    @Override
    public void onClick(View arg0) {
        if (arg0.getId() == R.id.buttonLogin2) {

            btnLogin.setEnabled(false);
            context = this;
            loginUser(context);
        }
    }

    private void loginUser(final Context context){
        try {

            Retrofit retrofit = RetrofitSingleton.getInstance(URI.BASE_URL);
            LoginService loginService = retrofit.create(LoginService.class);
            Call<AccessToken> authenticateCall = loginService.getAccessToken("password", currentUser.getEmail(), currentUser.getPassword());

            authenticateCall.enqueue(new Callback<AccessToken>() {
                @Override
                public void onResponse(Call<AccessToken> call, Response<AccessToken> response) {
                    // response.isSuccessful() is true if the response code is 2xx
                    if (response.isSuccessful()) {
                        accessToken = response.body();
                        sharedPreferencesHelper.putString("AccessToken", accessToken.getAccessToken());
                        Intent intent = new Intent(context, MainActivity.class);
                        context.startActivity(intent);

                    } else {
                        int statusCode = response.code();
                        // handle request errors yourself
                        ResponseBody errorBody = response.errorBody();

                        Context context = getApplicationContext();
                        CharSequence text = "Failed to login! " + statusCode;
                        int duration = Toast.LENGTH_LONG;

                        Toast.makeText(context, text, duration).show();
                    }
                }

                @Override
                public void onFailure(Call<AccessToken> call, Throwable t) {
                    // handle execution failures like no internet connectivity

                    Context context = getApplicationContext();
                    CharSequence text = "Failed to connect!";
                    int duration = Toast.LENGTH_LONG;

                    Toast.makeText(context, text, duration).show();
                }
            });
        }
        catch(Exception e){
            String s = e.toString();
        }
    }
}