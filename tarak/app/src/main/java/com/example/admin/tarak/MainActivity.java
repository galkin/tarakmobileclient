package com.example.admin.tarak;


import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.Context;
import android.content.OperationApplicationException;
import android.content.res.Configuration;
import android.database.Cursor;
import android.os.Bundle;
import android.os.RemoteException;
import android.provider.ContactsContract;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.prolificinteractive.materialcalendarview.CalendarDay;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import api.RetrofitSingleton;
import api.VisitService;
import model.CurrentDate;
import model.VisitList;
import model.VisitModel;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import util.SharedPreferencesHelper;
import api.URI;

public class MainActivity extends AppCompatActivity {
    public static ViewPager viewPager;
    public static VisitList visits = new VisitList();
    MainPagerAdapter mainPagerAdapter;
    Context context;
    private SharedPreferencesHelper sharedPreferencesHelper;
    public static CurrentDate currentDate;
    public static CalendarDay yDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = this;

        //current date settings
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        currentDate = new CurrentDate();
        currentDate.setDay(calendar.get(Calendar.DAY_OF_MONTH));
        //well + 1 because of JAVA
        currentDate.setMonth(calendar.get(Calendar.MONTH) + 1);
        currentDate.setYear(calendar.get(Calendar.YEAR));

        //set default language
        Locale locale = new Locale("ru");
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());

        //Toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Remove toolbar title and set logotip
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setIcon(R.drawable.logotip);

        //Add tabs
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.tab_contacts)));
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.tab_today)));
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.tab_week)));
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.tab_month)));
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.tab_year)));
        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);

        viewPager = (ViewPager) findViewById(R.id.pager);
        final MainPagerAdapter adapter = new MainPagerAdapter(getSupportFragmentManager(), tabLayout.getTabCount(), currentDate);
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        mainPagerAdapter = adapter;
        sharedPreferencesHelper = new SharedPreferencesHelper(this);

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {

            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                // Reloads pagerAdapter
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        renderData(calendar.get(Calendar.YEAR));

        //Write test contacts to phone contacts
        String[] testContacts = {"Test contact 11431", "Test contact 11432", "Test contact 11413"};
        Cursor phones = getApplicationContext().getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC");
        if(phones.getCount() == 0) {
            for(int i = 0; i < 3; i++) {
                WriteTestPhoneContact(testContacts[i], "0700-11-00-00", getApplicationContext());
                System.out.println("write contact " + testContacts[i]);
            }
        }
    }

    public void renderData(int year) {
        String accessToken = sharedPreferencesHelper.getString("AccessToken");
        String bearer = "Bearer " + accessToken;

        Retrofit retrofit = RetrofitSingleton.getInstance(URI.BASE_URL);
        VisitService visitService = retrofit.create(VisitService.class);

        Call<List<VisitModel>> call = visitService.getYearlyVisits(bearer, year);

        call.clone().enqueue(new Callback<List<VisitModel>>() {
            @Override
            public void onResponse(Call<List<VisitModel>> call, Response<List<VisitModel>> response) {
                // response.isSuccessful() is true if the response code is 2xx
                if (response.isSuccessful()) {
                    //get annual list of visits
                    List<VisitModel> list = response.body();
                    visits.addVisits(list);

                } else {
                    int statusCode = response.code();
                    // handle request errors yourself
                    ResponseBody errorBody = response.errorBody();

                    Context context = getApplicationContext();
                    CharSequence text = "Failed to get visit data! " + errorBody;
                    int duration = Toast.LENGTH_LONG;

                    Toast.makeText(context, text, duration).show();
                }
            }

            @Override
            public void onFailure(Call<List<VisitModel>> call, Throwable t) {
                // handle execution failures like no internet connectivity

                Context context = getApplicationContext();
                CharSequence text = "Failed to connect!";
                int duration = Toast.LENGTH_LONG;

                Toast.makeText(context, text, duration).show();
            }
        });
    }

    //Write test contacts to phone contacts
    public void WriteTestPhoneContact(String contactName, String number, Context cntx) {
        Context contetx = cntx;
        String strcontactName = contactName;
        String phoneNumber = number;

        ArrayList<ContentProviderOperation> cntProviderOperation = new ArrayList<ContentProviderOperation>();
        int contactIndex =  cntProviderOperation .size();

        // A raw contact will be inserted ContactsContract.RawContacts table in contacts database.
        cntProviderOperation.add(ContentProviderOperation.newInsert(ContactsContract.RawContacts.CONTENT_URI)
                .withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, null)
                .withValue(ContactsContract.RawContacts.ACCOUNT_NAME, null).build());

        //Contact name will be inserted in ContactsContract.Data table
        cntProviderOperation.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, contactIndex)
                .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME, strcontactName)
                .build());
        //Mobile number will be inserted in ContactsContract.Data table
        cntProviderOperation.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, contactIndex)
                .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, phoneNumber)
                .withValue(ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE).build());
        try
        {
            ContentProviderResult[] contentProresult = null;
            contentProresult = contetx.getContentResolver().applyBatch(ContactsContract.AUTHORITY, cntProviderOperation);
        }
        catch (RemoteException exp)
        {
            Log.v("Add test contacts error", "RemoteException");
        }
        catch (OperationApplicationException exp)
        {
            Log.v("Add test contacts error", "OperationApplicationException");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }
}
