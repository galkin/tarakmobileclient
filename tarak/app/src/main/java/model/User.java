package model;

import android.databinding.ObservableField;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;

import org.json.JSONException;
import org.json.JSONObject;

import util.TextWatcherAdapter;

/**
 * Created by andy on 4/30/2016.
 */
public class User {
    private ObservableField<String> firstName = new ObservableField<>("");
    private ObservableField<String> lastName = new ObservableField<>("");
    private ObservableField<String> email = new ObservableField<>("");
    private ObservableField<String> password = new ObservableField<>("");
    private ObservableField<String> phoneNumber = new ObservableField<>("");
    private int id;

    public User(){
        this.id = 0;
    }

    public String getEmail() {
        return email.get();
    }

    public void setEmail(String email) {
        this.email.set(email);
    }

    public String getFirstName() {
        return firstName.get();
    }

    public void setFirstName(String firstName) {
        this.firstName.set(firstName);
    }

    public String getLastName() {
        return lastName.get();
    }

    public void setLastName(String lastName) {
        this.lastName.set(lastName);
    }

    public String getPassword() {
        return password.get();
    }

    public void setPassword(String password) {
        this.password.set(password);
    }

    public String getPhoneNumber() {
        return phoneNumber.get();
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber.set(phoneNumber);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public TextWatcher firstNameWatcher = new TextWatcherAdapter() {
        @Override public void afterTextChanged(Editable s) {
            if (!TextUtils.equals(firstName.get(), s.toString())) {
                firstName.set(s.toString());
            }
        }
    };

    public TextWatcher lastNameWatcher = new TextWatcherAdapter() {
        @Override public void afterTextChanged(Editable s) {
            if (!TextUtils.equals(lastName.get(), s.toString())) {
                lastName.set(s.toString());
            }
        }
    };

    public TextWatcher emailWatcher = new TextWatcherAdapter() {
        @Override public void afterTextChanged(Editable s) {
            if (!TextUtils.equals(email.get(), s.toString())) {
                email.set(s.toString());
            }
        }
    };

    public TextWatcher phoneNumberWatcher = new TextWatcherAdapter() {
        @Override public void afterTextChanged(Editable s) {
            if (!TextUtils.equals(phoneNumber.get(), s.toString())) {
                phoneNumber.set(s.toString());
            }
        }
    };

    public TextWatcher passwordWatcher = new TextWatcherAdapter() {
        @Override public void afterTextChanged(Editable s) {
            if (!TextUtils.equals(password.get(), s.toString())) {
                password.set(s.toString());
            }
        }
    };

    public String toJSON() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("Id", getId());
            jsonObject.put("FirstName",  getFirstName());
            jsonObject.put("LastName",  getLastName());
            jsonObject.put("Email", getEmail());
            jsonObject.put("PhoneNumber", getPhoneNumber());
            jsonObject.put("Password", getPassword());
            //TODO: Set confirm password field
            jsonObject.put("ConfirmPassword", getPassword());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject.toString();
    }

}
