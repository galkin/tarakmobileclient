package model;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.admin.tarak.R;

import java.util.ArrayList;
import java.util.List;

import model.SelectUser;


public class SelectUserAdapter extends BaseAdapter {

    public List<SelectUser> users;
    private ArrayList<SelectUser> arraylist;
    Context _c;
    ImageView contactPic;
    TextView name, phone;

    public SelectUserAdapter(List<SelectUser> selectUsers, Context context) {
        users = selectUsers;
        _c = context;
        this.arraylist = new ArrayList<SelectUser>();
        this.arraylist.addAll(users);
    }

    @Override
    public int getCount() {
        return users.size();
    }

    @Override
    public Object getItem(int i) {
        return users.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        View view = convertView;
        if (view == null) {
            LayoutInflater li = (LayoutInflater) _c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = li.inflate(R.layout.contact_list_item, null);
         } else {
            view = convertView;
         }

        name = (TextView) view.findViewById(R.id.name);
        phone = (TextView) view.findViewById(R.id.number);
        contactPic = (ImageView) view.findViewById(R.id.contact_pic);

        final SelectUser data = (SelectUser) users.get(i);
        name.setText(data.getName());
        phone.setText(data.getPhone());

        // Set image if exists
        try {

            if (data.getThumb() != null) {
                contactPic.setImageBitmap(data.getThumb());
            } else {
                contactPic.setImageResource(R.drawable.contact);
            }
        } catch (OutOfMemoryError e) {
            // Add default picture
            contactPic.setImageDrawable(this._c.getDrawable(R.drawable.contact));
            e.printStackTrace();
        }

        view.setTag(data);
        return view;
    }
}