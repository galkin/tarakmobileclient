package model;

/**
 * Created by andy on 5/26/2016.
 */
public class VisitModel {
    private int Id;
    private String TimeZoneInfo;

    private String Description = "";
    private String CheckInDate = "";

    public String getCheckOutDate() {
        return CheckOutDate;
    }

    public void setCheckOutDate(String checkOutDate) {
        CheckOutDate = checkOutDate;
    }

    public String getCheckInDate() {
        return CheckInDate;
    }

    public void setCheckInDate(String checkInDate) {
        CheckInDate = checkInDate;
    }

    public String getTimeZoneInfo() {
        return TimeZoneInfo;
    }

    public void setTimeZoneInfo(String timeZoneInfo) {
        TimeZoneInfo = timeZoneInfo;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getVisitorName() {
        return VisitorName;
    }

    public void setVisitorName(String visitorName) {
        VisitorName = visitorName;
    }

    private String CheckOutDate = "";
    private String VisitorName = "";

    public VisitModel(){
        this.Id = 0;
    }

}
