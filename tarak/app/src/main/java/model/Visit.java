/**
 * Created by andy on 12/16/2015.
 */

package model;

import android.databinding.ObservableField;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;

import org.json.JSONException;
import org.json.JSONObject;

import util.TextWatcherAdapter;

public class Visit {

    private int id;
    private String timeZoneInfo;

    private ObservableField<String> description = new ObservableField<>("");
    private ObservableField<String> visitDate = new ObservableField<>("");
    private ObservableField<String> checkInTime = new ObservableField<>("");
    private ObservableField<String> checkOutTime = new ObservableField<>("");
    private ObservableField<String> visitorName = new ObservableField<>("");

    public Visit(){
        this.id = 0;
    }

    public String getVisitorName() {
        return this.visitorName.get();
    }

    public int getId() {
        return id;
    }

    public String getTimeZoneInfo() {
        return timeZoneInfo;
    }

    public String getCheckInTime() {
        return checkInTime.get();
    }

    public String getCheckOutTime() {
        return checkOutTime.get();
    }

    public String getVisitDate() {
        return visitDate.get();
    }

    public String getDescription() {
        return this.description.get();
    }

    public void setVisitorName(String visitorName) {
        this.visitorName.set(visitorName);
    }

    public void setTimeZoneInfo(String timeZoneInfo) {
        this.timeZoneInfo = timeZoneInfo;
    }

    public void setCheckInTime(String checkInDate) {
        this.checkInTime.set(checkInDate);
    }

    public void setCheckOutTime(String checkOutDate) {
        this.checkOutTime.set(checkOutDate);
    }

    public void setVisitDate(String visitDate) {
        this.visitDate.set(visitDate);
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setDescription(String  description) {
        this.description.set(description);
    }

    public TextWatcher descriptionWatcher = new TextWatcherAdapter() {
        @Override public void afterTextChanged(Editable s) {
            if (!TextUtils.equals(description.get(), s.toString())) {
                description.set(s.toString());
            }
        }
    };

    public TextWatcher visitorNameWatcher = new TextWatcherAdapter() {
        @Override public void afterTextChanged(Editable s) {
            if (!TextUtils.equals(visitorName.get(), s.toString())) {
                visitorName.set(s.toString());
            }
        }
    };

    public TextWatcher visitDateWatcher = new TextWatcherAdapter() {
        @Override public void afterTextChanged(Editable s) {
            if (!TextUtils.equals(visitDate.get(), s.toString())) {
                visitDate.set(s.toString());
            }
        }
    };

    public TextWatcher checkInTimeWatcher = new TextWatcherAdapter() {
        @Override public void afterTextChanged(Editable s) {
            if (!TextUtils.equals(checkInTime.get(), s.toString())) {
                checkInTime.set(s.toString());
            }
        }
    };

    public TextWatcher checkOutTimeWatcher = new TextWatcherAdapter() {
        @Override public void afterTextChanged(Editable s) {
            if (!TextUtils.equals(checkOutTime.get(), s.toString())) {
                checkOutTime.set(s.toString());
            }
        }
    };

    public String toJSON() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("Id", getId());
            jsonObject.put("Description",  getDescription());
            jsonObject.put("CheckInDate",  getVisitDate() + " " + getCheckInTime()  + ":00");
            jsonObject.put("CheckOutDate", getVisitDate() + " " + getCheckOutTime() + ":00");
            jsonObject.put("TimeZoneInfo", getTimeZoneInfo());
            jsonObject.put("VisitorName",  getVisitorName());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject.toString();
    }
}
