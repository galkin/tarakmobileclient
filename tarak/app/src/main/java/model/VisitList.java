package model;

import android.text.format.Time;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import util.DateHelper;
import util.ParseTime;

/**
 * Created by andy on 12/16/2015.
 */
public class VisitList {
    public VisitList(){
        visits = new ArrayList<>();
    }

    public List<Visit> getVisits() {
        return visits;
    }

    public void setVisits(List<Visit> visits) {
        this.visits = visits;
    }

    public void addVisits(List<VisitModel> visits) {
        List<Visit> list = new ArrayList<>();

        for(VisitModel visitModel : visits){
            SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat timeFormatter = new SimpleDateFormat("HH:mm");
            String checkInDate = visitModel.getCheckInDate();
            String checkOutDate = visitModel.getCheckOutDate();

            String[] dateFormats = DateHelper.getDateFromats();
            Date CheckInDate = DateHelper.parseDate(checkInDate, dateFormats);
            Date CheckOutDate = DateHelper.parseDate(checkOutDate, dateFormats);

            String checkInTime = timeFormatter.format(CheckInDate);
            String checkOutTime = timeFormatter.format(CheckOutDate);
            String visitDate = dateFormatter.format(CheckInDate);

            Visit visit = new Visit();
            visit.setId(visitModel.getId());
            visit.setTimeZoneInfo(visitModel.getTimeZoneInfo());
            visit.setDescription(visitModel.getDescription());
            visit.setVisitorName(visitModel.getVisitorName());
            visit.setVisitDate(visitDate);
            visit.setCheckInTime(checkInTime);
            visit.setCheckOutTime(checkOutTime);

            list.add(visit);
        }

        this.visits = list;
    }

    public void addVisit(Visit visit) {
        this.visits.add(visit);
    }

    public void addVisit(VisitModel visitModel) {
        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat timeFormatter = new SimpleDateFormat("HH:mm");
        String checkInDate = visitModel.getCheckInDate();
        String checkOutDate = visitModel.getCheckOutDate();

        String[] dateFormats = DateHelper.getDateFromats();
        Date CheckInDate = DateHelper.parseDate(checkInDate, dateFormats);
        Date CheckOutDate = DateHelper.parseDate(checkOutDate, dateFormats);

        String checkInTime = timeFormatter.format(CheckInDate);
        String checkOutTime = timeFormatter.format(CheckOutDate);
        String visitDate = dateFormatter.format(CheckInDate);

        Visit visit = new Visit();
        visit.setId(visitModel.getId());
        visit.setTimeZoneInfo(visitModel.getTimeZoneInfo());
        visit.setDescription(visitModel.getDescription());
        visit.setVisitorName(visitModel.getVisitorName());
        visit.setVisitDate(visitDate);
        visit.setCheckInTime(checkInTime);
        visit.setCheckOutTime(checkOutTime);

        this.visits.add(visit);
    }

    public void deleteVisit(Visit visit) {
        this.visits.remove(visit);
        Iterator<Visit> itr = this.visits.iterator();
        while (itr.hasNext()) {
            Visit v = itr.next();
            if (v.getId() == visit.getId()) {
                itr.remove();
            }
        }
    }

    private List<Visit> visits;

    public Visit getVisitById(int id)
    {
        for (int i = 0; i < visits.size(); i++) {
            if(visits.get(i).getId() == id){
                return visits.get(i);
            }
        }

        return null;
    }

    public Visit getVisitByTimeFrame(int currentMonth, int currentDay, String checkInHour, String checkOutHour)
    {
        String[] dateFormats = DateHelper.getDateFromats();

        Time in = ParseTime.parseHHMM(checkInHour);
        Time out = ParseTime.parseHHMM(checkOutHour);

        for (int i = 0; i < visits.size(); i++) {
            Visit visit = visits.get(i);
            String visitDateTime = visit.getVisitDate() + " " + visit.getCheckInTime()  + ":00";
            Date date = DateHelper.parseDate(visitDateTime, dateFormats);

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            int hour = calendar.get(Calendar.HOUR_OF_DAY);
            int day = calendar.get(Calendar.DAY_OF_MONTH);
            //we need to add 1 to make month number more understandable
            int month = calendar.get(Calendar.MONTH) + 1;

            if (currentMonth == month && currentDay == day && in.hour <= hour && out.hour >= hour)
                return visits.get(i);
        }

        return null;
    }
}
