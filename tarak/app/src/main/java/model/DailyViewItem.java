package model;


public class DailyViewItem  {

    private String hour;
    private int icon;
    private String message;
    private String checkinHour;

    public String getCheckoutHour() {
        return checkoutHour;
    }

    public void setCheckoutHour(String checkoutHour) {
        this.checkoutHour = checkoutHour;
    }

    public String getCheckinHour() {
        return checkinHour;
    }

    public void setCheckinHour(String checkinHour) {
        this.checkinHour = checkinHour;
    }

    private String checkoutHour;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DailyViewItem(String hour, int icon, String message, String checkinHour, String checkoutHour) {
        this.hour = hour;
        this.icon = icon;
        this.message = message;

        this.checkinHour = checkinHour;
        this.checkoutHour = checkoutHour;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

}