package api;

import model.AccessToken;
import model.User;
import model.UserModel;
import model.VisitModel;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by andy on 5/21/2016.
 */
public interface LoginService {
    @POST("Token")
    @FormUrlEncoded
    @Headers({"Accept: application/json", "Content-Type: x-www-form-urlencoded"})
    Call<AccessToken> getAccessToken(@Field("grant_type") String grant_type,
                                     @Field("username") String username,
                                     @Field("password") String password);

    @Headers({"Accept: application/json", "Content-Type: application/json"})
    @POST("api/v1/Account/Register")
    Call<Void> createUser(@Body UserModel user);
}