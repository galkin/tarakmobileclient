package api;

import java.util.List;

import model.VisitModel;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

/**
 * Created by andy on 5/25/2016.
 */
public interface VisitService {
    @GET("api/v1/YearlyVisits/{year}")
    Call<List<VisitModel>> getYearlyVisits(@Header("Authorization") String bearer, @Path("year") int year);

    @Headers({"Accept: application/json", "Content-Type: application/json"})
    @POST("api/v1/Visits")
    Call<Void> addVisit(@Header("Authorization") String bearer, @Body VisitModel visit);

    @Headers({"Accept: application/json"})
    @PUT("api/v1/Visits")
    Call<Void> updateVisit(@Header("Authorization") String bearer, @Body VisitModel visit);

    @Headers("Content-Type: text/not-plain")
    @DELETE("api/v1/Visits/{visitId}")
    Call<Void> deleteVisit(@Header("Authorization") String bearer, @Path("visitId") int visitId);
}
