package api;
        import java.util.concurrent.TimeUnit;
        import okhttp3.OkHttpClient;
        import okhttp3.logging.HttpLoggingInterceptor;
        import retrofit2.Retrofit;
        import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitSingleton {
    private static Retrofit restAdapter = null;

    private RetrofitSingleton() {

    }

    public static Retrofit getInstance(String url) {

        OkHttpClient httpClient = new OkHttpClient.Builder().build();

        if (restAdapter == null) {
            restAdapter = new Retrofit.Builder()
                    .baseUrl(url)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(httpClient)
                    .build();
        }
        return restAdapter;
    }
}