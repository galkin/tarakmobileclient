package api;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

/**
 * Created by andy on 12/16/2015.
 */
public class URI
{
    public static final String BASE_URL = "http://tarakserver.azurewebsites.net/";
    public static final String BASE_URL_local = "http://192.168.1.101/TarakServer/";
}
