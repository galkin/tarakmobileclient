package util;

import android.text.format.Time;

/**
 * Created by andy on 3/24/2016.
 */
public class ParseTime
{
    /**
     * Returns the time represented by the <tt>time</tt> String.
     * @param time A String formatted like "HH:MM"
     * @return A Time object representing the time, or null if the String could not be parsed.
     */
    public static Time parseHHMM(String time)
    {
        Time t = new Time();
        // if the format is not "HH:MM" we return null
        try
        {
            String hours = time.substring(0, 2);
            String minutes = time.substring(3);

            t.hour = Integer.parseInt(hours);
            t.minute = Integer.parseInt(minutes);
        }
        catch (Exception e)
        {
            return null;
        }

        return t;
    }
}