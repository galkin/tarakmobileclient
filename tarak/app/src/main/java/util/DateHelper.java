package util;

import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by andy on 2/7/2016.
 */
public class DateHelper {
    public static String[] getDateFromats(){
        String[] dateFromats = new String[] {
                "yyyy-MM-dd'T'HH:mm:ss",
                "yyyy-MM-dd HH:mm:ss",
                "yyyy-MM-dd",
                "yyyy-MM-dd HH:mm:ss.SSSZ",
                "yyyy-MM-dd'T'HH:mm:ss.SSSZ",
                "yyyy-MM-dd'T'HH:mm:ss.SSS",
                "YYYY-MM-DDThh:mm:ss"
        };

        return dateFromats;
    }

    public static Date parseDate(String dateString, String[] formats)
    {
        Date date = null;

        for (int i = 0; i < formats.length; i++)
        {
            String format = formats[i];
            SimpleDateFormat dateFormat = new SimpleDateFormat(format);

            try
            {
                // parse() will throw an exception if the given dateString doesn't match
                // the current format
                date = dateFormat.parse(dateString);
                break;
            }
            catch(Exception e)
            {
                Log.v("",e.toString());
            }
        }

        return date;
    }
}
