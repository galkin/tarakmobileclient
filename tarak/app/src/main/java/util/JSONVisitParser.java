package util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;

import model.Visit;
import model.VisitList;
import util.DateHelper;

/**
 * Created by andy on 12/16/2015.
 */
public class JSONVisitParser {
    public static VisitList getVisits(String data){

        VisitList visits = new VisitList();

        try{

            JSONArray jsonArray = new JSONArray(data);
            List<Visit> list = new ArrayList<>();
            for (int i = 0; i < jsonArray.length(); ++i) {
                JSONObject rec = jsonArray.getJSONObject(i);
                list.add(getVisit(rec));
            }

            visits.setVisits(list);
            return visits;
        } catch(JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static Visit getVisit(String data) {
        try {
            if (data == null)
                return new Visit();

            JSONObject rec = new JSONObject(data);
            return getVisit(rec);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    private static Visit getVisit(JSONObject rec) {
        try {
            SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat timeFormatter = new SimpleDateFormat("HH:mm");

            int id = rec.getInt("Id");
            String checkInDate = rec.getString("CheckInDate");
            String checkOutDate = rec.getString("CheckOutDate");
            String description = rec.getString("Description");
            String visitorName = rec.getString("VisitorName");

            String[] dateFormats = DateHelper.getDateFromats();
            Date CheckInDate = DateHelper.parseDate(checkInDate, dateFormats);
            Date CheckOutDate = DateHelper.parseDate(checkOutDate, dateFormats);

            String checkInTime = timeFormatter.format(CheckInDate);
            String checkOutTime = timeFormatter.format(CheckOutDate);
            String visitDate = dateFormatter.format(CheckInDate);

            Visit visit = new Visit();
            visit.setCheckInTime(checkInTime);
            visit.setCheckOutTime(checkOutTime);
            visit.setDescription(description);
            visit.setVisitorName(visitorName);
            visit.setVisitDate(visitDate);
            visit.setId(id);

            return visit;
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }
}
